
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;

public class Test1Definition {
    
	@Given("Go to file:///Users/presidio/Downloads/front-end/index.html")
	public void go_to_file_Users_presidio_Downloads_front_end_index_html() {
		// write code here that turns the phrase above into concrete actions
		throw new io.cucumber.java.PendingException();
	}
	@When("I entered {string} in name")
	public void i_entered_in_name(String value) {
		// write code here that turns the phrase above into concrete actions
		nameTextBox.sendKeys(value);
		throw new io.cucumber.java.PendingException();
	}
	@And("I entered {string} in email")
	public void i_entered_in_email(String value) {
		// write code here that turns the phrase above into concrete actions
		emailTextBox.sendKeys(value);
		throw new io.cucumber.java.PendingException();
	}
	@And("I selected {string} in qualification")
	public void i_selected_in_qualification(String value) {
		// write code here that turns the phrase above into concrete actions
		qualificationDropDown.sendKeys(value);
		throw new io.cucumber.java.PendingException();
	}
	@And("I chose {string} in gender")
	public void i_chose_in_gender(String value) {
		// write code here that turns the phrase above into concrete actions
		genderRadioButton.sendKeys(value);
		throw new io.cucumber.java.PendingException();
	}
	@And("I clicked on {string} button")
	public void i_clicked_on_button(String value) {
		// write code here that turns the phrase above into concrete actions
		CancelButton.click();
		throw new io.cucumber.java.PendingException();
	}


}

