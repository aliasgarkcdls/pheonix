
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import io.cucumber.java.en.Then;

public class TestingDefinition {

	@Steps
	TestingPage testingPage;

	@Given("Go to https://www.amazon.in/")
	public void go_to_https_www_amazon_in_() {
		testingPage.open();
	}

	@When("I entered {string} in field-keywords")
	public void i_entered_in_field_keywords(String value) {
		testingPage.twotabsearchtextboxImplementation(value);
	}

	@And("I clicked on Go button")
	public void i_clicked_on_go_button() {
		testingPage.nav_search_submit_buttonImplementation();
	}

	@Then("I am on the https://www.amazon.in/s?k=watches+for+men&crid=21RCF1KHGMZL3&sprefix=watches+for+men%2Caps%2C201&ref=nb_sb_noss_1 page")
	public void I_am_on_the_https_www_amazon_in_s_k_watches_for_men_crid_21RCF1KHGMZL3_sprefix_watches_for_men_2Caps_2C201_ref_nb_sb_noss_1_page() {
		System.out.println("https_www_amazon_in_s_k_watches_for_men_crid_RCF_KHGMZL_sprefix_watches_for_men_Caps_C_ref_nb_sb_noss_Implementation");
	}

	@And("I clicked on MK-2029W Black Dial Day & Date Watch for Men hyperlink")
	public void i_clicked_on_mk_2029w_black_dial_day_date_watch_for_men_hyperlink() {
		testingPage.a_size_base_plus_a_color_base_a_text_normalImplementation();
	}

	@Then("I am on the https://www.amazon.in/Lorenz-MK-2029W-Black-Dial-Watch/dp/B07NWQ81HN/ref=sr_1_1_sspa?crid=21RCF1KHGMZL3&keywords=watches+for+men&qid=1673267431&sprefix=watches+for+men%2Caps%2C201&sr=8-1-spons&sp_csd=d2lkZ2V0TmFtZT1zcF9hdGY&psc=1 page")
	public void I_am_on_the_https_www_amazon_in_Lorenz_MK_2029W_Black_Dial_Watch_dp_B07NWQ81HN_ref_sr_1_1_sspa_crid_21RCF1KHGMZL3_keywords_watches_for_men_qid_1673267431_sprefix_watches_for_men_2Caps_2C201_sr_8_1_spons_sp_csd_d2lkZ2V0TmFtZT1zcF9hdGY_psc_1_page() {
		System.out.println("https_www_amazon_in_Lorenz_MK_W_Black_Dial_Watch_dp_B_NWQ_HN_ref_sr_sspa_crid_RCF_KHGMZL_keywords_watches_for_men_qid_sprefix_watches_for_men_Caps_C_sr_spons_sp_csd_d_lkZ_V_TmFtZT_zcF_hdGY_psc_Implementation");
	}

	@And("I clicked on 3.0 out of 5 stars hyperlink")
	public void i_clicked_on_3_0_out_of_5_stars_hyperlink() {
		testingPage.a_icon_a_icon_star_a_star_Implementation();
	}



}

