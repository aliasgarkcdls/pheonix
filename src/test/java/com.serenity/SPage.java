
import org.openqa.selenium.WebElement;

public class R7Page extends PageObject {
	@FindBy(id = "name")
	WebElement nameTextBox;

	@FindBy(id = "email")
	WebElement emailTextBox;

	@FindBy(xpath = "/html[1]/body[1]/div[@class='center']/div[@class='center']/div[1]/input[1]")
	WebElement CancelButton;


}