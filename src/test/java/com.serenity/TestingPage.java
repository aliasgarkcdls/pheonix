
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Step;

public class TestingPage extends PageObject {

	@FindBy(id = "twotabsearchtextbox")
	WebElement twotabsearchtextboxTextBox;

	@FindBy(id = "nav-search-submit-button")
	WebElement nav_search_submit_buttonButton;

	@FindBy(class = "a-size-base-plus a-color-base a-text-normal")
	WebElement a_size_base_plus_a_color_base_a_text_normalHyperlink;

	@FindBy(class = "a-icon a-icon-star a-star-3")
	WebElement a_icon_a_icon_star_a_star_Hyperlink;

	@Step("Enter twotabsearchtextbox")
	public void twotabsearchtextboxImplementation(String value) {
		twotabsearchtextboxTextBox.sendKeys(value);
	}

	@Step("Click nav-search-submit-button Button")
	public void nav_search_submit_buttonImplementation() {
		nav_search_submit_buttonButton.click();
	}

	@Step("Click a-size-base-plus a-color-base a-text-normal Hyperlink")
	public void a_size_base_plus_a_color_base_a_text_normalImplementation() {
		a_size_base_plus_a_color_base_a_text_normalHyperlink.click();
	}

	@Step("Click a-icon a-icon-star a-star-3 Hyperlink")
	public void a_icon_a_icon_star_a_star_Implementation() {
		a_icon_a_icon_star_a_star_Hyperlink.click();
	}


}